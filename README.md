# Articles pour le site de l'ATCPB

Dans ce répertoire, vous trouverez les différents articles de l'ATCPB
à mettre sur le site internet. Le fichier list_todo.org contient les
idées d'articles que j'ai pour le moment. N'hésitez pas à ajouter des
idées ou soumettre des articles.

Les fichiers que j'écris sont au format org-mode, que vous pourrez
ouvrir avec le programme [org-mode](http://orgmode.org/fr/), ou avec
un éditeur de texte classique. C'est une partie de l'éditeur de texte
[GNU Emacs](http://www.gnu.org/software/emacs/). Si vous souhaitez
écrire des articles supplémentaires, je vous invite à utiliser
[org-mode](http://orgmode.org/fr/) ou *Markdown*. Voici un
[tutorial](http://markdowntutorial.com/). Cela permettra une
intégration plus simple au site de l'[ATCPB](http://www.atcpb.fr/). Si
vous n'avez jamais entendu parler de ces langages, je vous conseille
d'utiliser *Markdown*. Vous pourrez tester la syntaxe utilisée
[ici](http://dillinger.io).

Vous trouverez certainement que le fichier `list_todo.org` est très
indigeste à lire. C'est normal! J'utilise de mon côté **Emacs** qui
colore et change la taille de certaines parties du fichier, ce qui
rend la lecture beaucoup plus simple pour moi.

J'exporterai les articles au format pdf pour que cela soit plus simple
à regarder. Par la suite, ils seront intégrés au site de l'ATCPB pour
avoir des pages interactives.

# Pour utiliser ce répertoire

L'utilisation de ce répertoire fera l'objet d'une page (une
présentation plus un tutoriel) parmis celles que je ferai. Cependant,
afin que vous pouviez commencer à l'utiliser, voilà les bases des
bases :

* Créer un compte sur [BitBucket](https://bitbucket.org/)
* M'envoyer un mail (marin.gilles@u-bourgogne.fr) ou un message sur
BitBucket (marin_gilles) pour que je vous ajoute à l'équipe ATCPB
sur le site, en indiquant votre identifiant BitBucket (**Ne me
communiquez pas votre mot de passe !!!**)
* Une fois ajouté, vous pourrez aller dans le répertoire `ATCPB
Articles`, puis dans `Source` (Barre de navigation sur la gauche de
l'écran). Vous trouverez içi tout le travail déjà effectué.
* Pour commencer un article, cliquez sur `New file` en haut à droite
de l'écran. Une zone de texte apparaîtra. Tapez votre texte (de
préférence en _Markdown_), puis une fois terminé, cliquez sur `Commit`
en dessous de la zone de texte.
* Et voilà, l'article est créé. Il ne restera qu'à l'insérer dans le
site.

Si vous voulez en savoir plus sur l'utilisation de ce répertoire, il
s'agit d'un répertoire de _contrôle de version_ nommé **Git**. Vous
pourrez trouver plus d'infos dans ce
[tutoriel](http://openclassrooms.com/courses/gerer-son-code-avec-git-et-github),
et vous pourrez essayer sur ce
[site](https://try.github.io/levels/1/challenges/1) en anglais.
** Attention ** : Si vous n'êtes pas habitués à la ligne de commande,
ca va piquer les yeux! Je vais faire le plus vite possible le tutoriel
avec l'interface graphique pour que tout le monde puisse l'utiliser.